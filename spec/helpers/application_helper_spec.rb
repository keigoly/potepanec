require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    subject { full_title(title) }

    context "引数に値がある場合" do
      let(:title) { "Item" }

      it { is_expected.to eq "Item - BIGBAG Store" }
    end

    context "引数に値がない場合" do
      let(:title) { "" }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数がnilである場合" do
      let(:title) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end
  end
end
