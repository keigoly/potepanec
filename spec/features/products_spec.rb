require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:other_product) { create(:product) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:taxon2) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:undisplayed_related_product) { create(:product, taxons: [taxon]) }
  let!(:no_related_product) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario 'taxonが紐付く場合、商品が属するカテゴリー一覧へ戻ること' do
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario 'taxonが紐付かない場合、トップページへ戻ること' do
    visit potepan_product_path(other_product.id)
    expect(page).to have_link "一覧ページへ戻る"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_path
  end

  it '商品画面を表示する' do
    # productの情報が表示されること
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content product.description
    within '.productsContent' do
      # 非関連商品が表示されないこと
      expect(page).to have_no_content no_related_product.name
      # １〜４つ目の関連商品が表示されること
      0.upto(3).each do |index|
        expect(page).to have_content related_products[index].name
        expect(page).to have_content related_products[index].price
      end
      # 関連しない商品は表示しないこと
      expect(page).to have_no_content no_related_product.name
    end
  end
end
