require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let!(:taxon) { create(:taxon, name: "T-shirts") }
  let!(:product) { create(:product, name: "sample", price: "12.34", taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  it "商品名を正確に表示しているかどうか" do
    expect(response.body).to include "sample"
  end

  it "商品の表示価格を正しく返している" do
    expect(response.body).to include "$12.34"
  end

  it "リクエストが成功した場合のレスポンスが返ってくる" do
    expect(response).to have_http_status "200"
  end
end
