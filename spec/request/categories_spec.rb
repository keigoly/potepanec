require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, name: "T-shirts") }
  let!(:product) { create(:product, name: "Nike", taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "HTTPステータスコードが要求に応じた情報が返されるかどうか" do
    expect(response).to have_http_status "200"
  end

  it "期待する商品名が表示されるかどうか" do
    expect(response.body).to include "T-shirts"
    expect(response.body).to include "Nike"
  end
end
