require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }

  describe "related_products" do
    subject { product.related_products }

    it '関連商品のみ表示されること' do
      expect(product.related_products).to match_array(related_product)
    end

    context "関連商品以外は表示させないこと" do
      let!(:not_related_product) { create(:product, taxons: [taxon1]) }
      let(:taxon1) { create(:taxon) }

      it { is_expected.to eq [] }
    end
  end
end
